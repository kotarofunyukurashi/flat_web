<?php
/*
Template Name: お問い合わせページ
*/
?>

<?php get_header(); ?>

<section id="contact">
    <div class="contact-wrap">
        <div class="title-wrap">
            <h2>お問い合わせ</h2>
        </div>
        <div class="contents-wrap">
            <?php echo do_shortcode('[contact-form-7 id="6" title="お問い合わせ"]'); ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>

