<?php
/*
Template Name: お申し込みページ
*/
?>

<?php get_header(); ?>
<section id="application">
    <div class="title-wrap">
        <h2>お申し込み</h2>
    </div>
    <div class="contents-wrap">
        <?php echo do_shortcode('[contact-form-7 id="15" title="お申し込み"]'); ?>
    </div>
</section>
<?php get_footer(); ?>
