$(function () {
    $('.question').click(function () {
        $answer = $(this).next('.answer');
        $rotate = $(this).children('i.micon_2');
        if ($answer.hasClass('hide')) {
            $answer.slideDown();
            $answer.removeClass('hide');
            $rotate.addClass('rotate-arrow');
            $answer.addClass('clicked_faq clicked_answer');
            $(this).addClass('clicked_faq clicked_question');

        } else {
            $answer.slideUp();
            $answer.addClass('hide');
            $rotate.removeClass('rotate-arrow');
            $answer.addClass('hide');
            $(this).removeClass('clicked_faq clicked_question');
        }
    });

    var slick = $('#mainSlide').slick({
        appendArrows: $('#arrows')

    });

    //以下は必要ないと思う
    $('.slick-next').on('click', function () {
        slick.slickNext();
    });
    $('.slick-prev').on('click', function () {
        slick.slickPrev();
    });

    //【スライダー】サービス内容 & 制作の流れ
    $('.slider').slick({
        centerMode: true,
        centerPadding: '0px',
        focusOnSelect: true,
        dots: true,
        accessibility: true,
        initialSlide: 0,
        infinite: true,
        speed: 500,
        variableWidth: true,
        focusOnSelect: true,
        prevArrow: '<div class="slider-arrow slider-prev"><img src="http://kurashi-develop.sakura.ne.jp/lightweb_kurashi/wp-content/themes/kurashitech-lightweb/img/slider/1.png"></div>',
        nextArrow: '<div class="slider-arrow slider-next"><img src="http://kurashi-develop.sakura.ne.jp/lightweb_kurashi/wp-content/themes/kurashitech-lightweb/img/slider/2.png"></div>',
        responsive: [{
            breakpoint: 480,
            settings:{
                centerPadding: '0px',
                focusOnSelect: true,
                centerMode: true,
                variableWidth: false
            }
        }]
    });

    //【スライダー】選べるデザイン
    $('.design-wrap').slick({
        centerMode: true,
        centerPadding: '0px',
        focusOnSelect: true,
        dots: true,
        accessibility: true,
        initialSlide: 0,
        infinite: true,
        variableWidth: true,
        focusOnSelect: true,
        prevArrow: '<div class="slider-arrow slider-prev"><img src="http://kurashi-develop.sakura.ne.jp/lightweb_kurashi/wp-content/themes/kurashitech-lightweb/img/slider/1.png"></div>',
        nextArrow: '<div class="slider-arrow slider-next"><img src="http://kurashi-develop.sakura.ne.jp/lightweb_kurashi/wp-content/themes/kurashitech-lightweb/img/slider/2.png"></div>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    centerPadding: '0px',
                    focusOnSelect: true,
                    speed: 500,
                    centerMode: true,
                    variableWidth: false,
                    dots: true
                }
            },
            {
                breakpoint: 480,
                settings:{
                    centerPadding: '0px',
                    focusOnSelect: true,
                    speed: 500,
                    centerMode: true,
                    variableWidth: false
                }
            }
        ]
    });

    //スクロールイベント
    $(window).scroll(function () {
        var animArray = [
            service,
            design,
            plan,
            production,
            faq
        ];
        while (animArray.length > 0) {
            animArray.shift()();
        }
    });

    var service = function service() {
        if ($('.slider').length) {
            var slider = $('.slider').offset().top;
            var scroll = $(window).scrollTop();
            var windowHeight = $(window).height();
            if (scroll > slider - windowHeight + 100) {
                $('.slider').addClass('fade_on');
            }
        }
    }

    var design = function design() {
        if ($('.design-wrap').length) {
            var design = $('.design-wrap').offset().top;
            var scroll = $(window).scrollTop();
            var windowHeight = $(window).height();
            if (scroll > design - windowHeight + 100) {
                $('.design-wrap').addClass('fade_on');
            }
        }
    }

    var plan = function plan() {
        if ($('#plan').length) {
            var price = $('.price-list').offset().top;
            var scroll = $(window).scrollTop();
            var windowHeight = $(window).height();
            if (scroll > price - windowHeight + 100) {
                $('.price-list').addClass('fade_on');
                $('.has-small').addClass('fade_on');
            }
        }
    }

    var production = function production() {
        if ($('#production').length) {
            var proSlider = $('#production .slider').offset().top;
            var scroll = $(window).scrollTop();
            var windowHeight = $(window).height();
            if (scroll > proSlider - windowHeight + 100) {
                $('#production .slider').addClass('fade_on');
            }
        }
    }

    var faq = function faq() {
        if ($('#faq').length) {
            var question = $('.qa-wrap').offset().top;
            var scroll = $(window).scrollTop();
            var windowHeight = $(window).height();
            if (scroll > question - windowHeight + 100) {
                var delay = 300;
                $('.questions > .qa-wrap').each(function (i) {
                    $(this).delay((i + 2) * delay).queue(function () {
                        $(this).addClass('fade_on');
                    });
                });
            }
        }
    }







})