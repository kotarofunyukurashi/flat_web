<?php get_header();?>
    <section id="top">
        <div class="bg-mask"></div>
        <h2>制作費無料。<br>必要なのは月々18,000円だけ。</h2>
        <p class="letter-color">中小企業・個人事業主に特化した<br>格安ホームページ制作</p>
    </section>
    <section id="service">
        <h2>サービス内容</h2>
        <p class="align-center letter-color">説明説明説明説明説明説明説明説明説明説明説明説明説明説明説明説明説明説明説明説明説明説明説明説明説明説明説明</p>
        <div class="slider fade_off">
            <div class="item">
                <div>
                    <div><img src="<?php echo get_template_directory_uri(); ?>/img/service/1.jpg" alt=""></div>
                    <div class="letter-wrap">
                        <h3>安いだけではありません</h3>
                        <p class="letter-color">取材・撮影・デザイン・文章作りなど、ホームページ制作に必要なすべてを
                            お任せいただけ、しかもすべてが無料です。

                            なにかと忙しい中小企業や個人事業主にピッタリ。
                            どんなにパソコンが苦手でも、インターネットが嫌いでも、大丈夫です。

                            めんどうなのは、嫌いでしょう？
                            琉球オフィスサービスなら、ほぼ丸投げでOKです。</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div>
                    <div><img src="<?php echo get_template_directory_uri(); ?>/img/service/2.jpg" alt=""></div>
                    <div class="letter-wrap">
                        <h3>安いだけではありません</h3>
                        <p class="letter-color">取材・撮影・デザイン・文章作りなど、ホームページ制作に必要なすべてを
                            お任せいただけ、しかもすべてが無料です。

                            なにかと忙しい中小企業や個人事業主にピッタリ。
                            どんなにパソコンが苦手でも、インターネットが嫌いでも、大丈夫です。

                            めんどうなのは、嫌いでしょう？
                            琉球オフィスサービスなら、ほぼ丸投げでOKです。</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div>
                    <div><img src="<?php echo get_template_directory_uri(); ?>/img/service/3.jpg" alt=""></div>
                    <div class="letter-wrap">
                        <h3>安いだけではありません</h3>
                        <p class="letter-color">取材・撮影・デザイン・文章作りなど、ホームページ制作に必要なすべてを
                            お任せいただけ、しかもすべてが無料です。

                            なにかと忙しい中小企業や個人事業主にピッタリ。
                            どんなにパソコンが苦手でも、インターネットが嫌いでも、大丈夫です。

                            めんどうなのは、嫌いでしょう？
                            琉球オフィスサービスなら、ほぼ丸投げでOKです。</p>
                    </div>
                </div>
            </div>
    </section>
    <section id="design">
        <h2>選べるデザイン</h2>
        <div class="wrapper">
            <div class="design-wrap fade_off">
                <div class="design">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/design/1.png" alt="スタンダードA">
                    <p class="slide-title-style">スタンダードAパターン</p>
                    <div class="button"><a href="https://newstd.net/" target="_blank">デモ画面を見る</a></div>
                </div>
                <div class="design">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/design/2.png" alt="スタンダードB">
                    <p class="slide-title-style">スタンダードBパターン</p>
                    <div class="button"><a href="https://wp-emanon.jp/emanon-business/" target="_blank">デモ画面を見る</a></div>
                </div>
                <div class="design">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/design/3.png" alt="コンサル">
                    <p class="slide-title-style">コンサルパターン</p>
                    <div class="button"><a href="http://demo-isotype.blue/majestic/" target="_blank">デモ画面を見る</a></div>
                </div>
                <div class="design">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/design/4.png" alt="おしゃれ">
                    <p class="slide-title-style">おしゃれパターン</p>
                    <div class="button"><a href="http://demo-isotype.blue/monoidea/" target="_blank">デモ画面を見る</a></div>
                </div>
                <div class="design">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/design/5.png" alt="カフェ">
                    <p class="slide-title-style">カフェパターン</p>
                    <div class="button"><a href="http://demo.crazy-wp.com/kanaka/" target="_blank">デモ画面を見る</a></div>
                </div>
            </div>
        </div>
    </section>
    <section id="plan">
        <h2>すべてコミ。<br>安心の定額制</h2>
        <p class="mini-title">※価格は税抜です。<br>※初回のみ契約手数料5,000円が必要です。</p>
        <div class="price-list fade_off">
            <div class="plan">
                <h3>プラン</h3>
                <div class="price">
                    <p>１ヶ月あたりの料金</p>
                    <p class="has-bigger">18,000円</p>
                </div>
            </div>
            <div class="service">
                <h3>対応サービス一覧</h3>
                <div class="services">
                    <div>
                        <p>基本サイト６ページ・取材・撮影</p><i class="material-icons">check_circle_outline</i>
                    </div>
                    <div>
                        <p>SSL(通信の暗号化)</p><i class="material-icons">check_circle_outline</i>
                    </div>
                    <div>
                        <p>独自ドメインメール</p><i class="material-icons">check_circle_outline</i>
                    </div>
                    <div>
                        <p>月５回程度の更新ご依頼</p><i class="material-icons">check_circle_outline</i>
                    </div>
                    <div>
                        <p>スマホ最適化</p><i class="material-icons">check_circle_outline</i>
                    </div>
                    <div>
                        <p>採用ページと連携</p><i class="material-icons">check_circle_outline</i>
                    </div>
                </div>
            </div>
        </div>
        <div class="price-list-sp">
            <div class="plan">
                <h3>プラン</h3>
                <div class="price">
                    <p>１ヶ月あたりの料金</p>
                    <p class="has-bigger">18,000円</p>
                </div>
            </div>
            <div class="service">
                <h3>対応サービス一覧</h3>
                <div class="services">
                    <div>
                        <p>基本サイト６ページ・取材・撮影</p><i class="material-icons">check_circle_outline</i>
                    </div>
                    <div>
                        <p>SSL(通信の暗号化)</p><i class="material-icons">check_circle_outline</i>
                    </div>
                    <div>
                        <p>独自ドメインメール</p><i class="material-icons">check_circle_outline</i>
                    </div>
                    <div>
                        <p>月５回程度の更新ご依頼</p><i class="material-icons">check_circle_outline</i>
                    </div>
                    <div>
                        <p>スマホ最適化</p><i class="material-icons">check_circle_outline</i>
                    </div>
                    <div>
                        <p>採用ページと連携</p><i class="material-icons">check_circle_outline</i>
                    </div>
                </div>
            </div>
        </div>
        <p class="has-small align-center fade_off">※ホームページ全体のリニューアル、ページ自体の新規制作、ページ全体を作り変えなどに関しては追加費用を頂きます。</p>
    </section>
    <section id="production">
        <h2>制作の流れ</h2>
        <p class="align-center letter-color">説明説明説明説明説明説明説明説明説明説明</p>
        <div class="slider fade_off">
            <div class="slide">
                <div class="contents">
                    <h2>1. まずはご連絡ください</h2>
                    <p class="letter-color">営業担当がお伺いして、ご要望のヒアリングとサービスのご説明を行います。ご都合の良い日時をご指定ください。
                        ネットやホームページに詳しくなくても、まったくご心配ありません。営業担当が持参するタブレットでいろんなサイトをご覧いただきながら、ご検討いただきますので、画面を見ながらお好みをお伝えいただくだけで、しっかりサポートいたします。
                    </p>
                </div>
            </div>
            <div class="slide">
                <div class="contents">
                    <h2>2. お申し込み。</h2>
                    <p class="letter-color">サービス内容をしっかり説明いたしますので、ご了承いただければお申し込みください。</p>
                </div>
            </div>
            <div class="slide">
                <div class="contents">
                    <h2>3. 取材にお伺いします。</h2>
                    <p class="letter-color">
                        会社・店舗の情報や事業内容、代表者のメッセージなどをヒアリング。商品や店舗の写真撮影も行います。そして、ホームページのカラーや雰囲気などの、お客様のお好みを伺います。営業担当者が持参するタブレットで、いろいろなホームページをご覧いただきながらイメージを固めていきます。おおまかなイメージをお伝えいただければ、あとは全部お任せください。
                    </p>
                </div>
            </div>
            <div class="slide">
                <div class="contents">
                    <h2>4. サンプルホームページを制作。</h2>
                    <p class="letter-color">
                        取材した内容をもとに、サンプルのホームページを制作します。<br>ご希望のカラーや雰囲気になっているかをご確認ください。<br>制作中や公開後の修正もどんどん受付いたしますので、全体の雰囲気を重点的にご確認ください。
                    </p>
                </div>
            </div>
            <div class="slide">
                <div class="contents">
                    <h2>5. ホームページ全体を制作</h2>
                    <p class="letter-color">サンプルのホームページのイメージがOKなら、続けてホームページ全体の制作にとりかかります。<br>お楽しみに、お待ちください。</p>
                </div>
            </div>
            <div class="slide">
                <div class="contents">
                    <h2>6. ホームページをウェブに公開。</h2>
                    <p class="letter-color">
                        全体の構成・文章等すべてをご確認いただき、問題がなければ独自ドメイン（サイトアドレス）を取得し、インターネット上に公開します。その際、検索エンジンにもアドレスを登録いたします。<br>名刺やチラシ、ブログ等にもホームページアドレスを記載して、活用しましょう。
                    </p>
                </div>
            </div>
            <div class="slide">
                <div class="contents">
                    <h2>7. どんどん、ご利用ください</h2>
                    <p class="letter-color">
                        更新はさまざまな方法で承っております。<br>琉球オフィスサービスなら、大半の更新作業を即日対応。また、都度費用の必要のない定額制ですので、お気軽にご依頼ください。</p>
                </div>
            </div>
        </div>



    </section>
    <section id="faq">
        <h2><span>FAQ</span><br>よくあるご質問</h2>
        <div class="questions">
            <div class="qa-wrap fade_off">
                <div class="question">
                    <i class="material-icons micon_1">
                        question_answer
                    </i>
                    <p>どんなホームページを制作できますか？</p>
                    <i class="material-icons micon_2">keyboard_arrow_right</i>
                </div>
                <div class="answer hide">
                    <p>目安6ページ程度の一般的なパソコン用ホームページ、スマホサイトが制作可能です。<br>
                        よく見られる機能はほとんど搭載していますので、ほとんどのお客様で不足なくご利用いただけます。<br>
                        制作実績もぜひご覧ください。<br>

                        初心者でもスマホやパソコンで自分で簡単に更新できる自社製CMSの搭載も可能です。<br>
                        ワードプレス等のオープンソースCMSでの制作も可能ですが、別途有償となります。</p>
                </div>
            </div>
            <div class="qa-wrap fade_off">
                <div class="question">
                    <i class="material-icons micon_1">
                        question_answer
                    </i>
                    <p>ホームページにはどんな機能がありますか？</p>
                    <i class="material-icons micon_2">keyboard_arrow_right</i>
                </div>
                <div class="answer hide">
                    <p>下記の基本機能はすべて無償で設置できます。<br>

                        自分で更新できるCMS機能<br>
                        メールフォーム設置<br>
                        営業・予定カレンダー設置<br>
                        フォトギャラリー設置<br>
                        独自ドメイン<br>
                        独自ドメインメール発行<br>
                        ブログ埋め込み<br>
                        Facebook / Twitter連携<br>
                        ほか</p>
                </div>
            </div>
            <div class="qa-wrap fade_off">
                <div class="question">
                    <i class="material-icons micon_1">
                        question_answer
                    </i>
                    <p>自分でパソコンを操作する必要はありますか？</p>
                    <i class="material-icons micon_2">keyboard_arrow_right</i>
                </div>
                <div class="answer hide">
                    <p>通常はすべて弊社にお任せいただけますので、パソコン等の操作は一切必要ありません。<br>
                        ご希望の場合は、弊社独自のホームページ更新機能を、ご自分で更新されたい箇所に埋め込みます。<br>
                        一般的な更新システム（CMS）よりも、非常にかんたんに、パソコンおよびスマホから更新が可能になります。</p>
                </div>
            </div>
            <div class="qa-wrap fade_off">
                <div class="question">
                    <i class="material-icons micon_1">
                        question_answer
                    </i>
                    <p>解約に違約金は必要ですか？</p>
                    <i class="material-icons micon_2">keyboard_arrow_right</i>
                </div>
                <div class="answer hide">
                    <p>必要ありません。
                        ただし、解約のご申告をいただいてから1ヶ月はご利用料金が発生いたします。

                    </p>
                </div>
            </div>
        </div>
        <div class="button-wrap"><a class="button"
                href="http://tayori.com/faq/f19e148800b203fed7d4f8770e024708742d570e" target="_blank">そのほか詳細を見る</a></div>
    </section>
    <section id="contact">
        <h2 class="pc"><span>CONTACT</span><br>お申し込み・お問い合わせ</h2>
        <h2 class="sp"><span>CONTACT</span><br>お申し込み<br>お問い合わせ</h2>
        <div class="contents-wrap">
            <div class="button-wrap">
                <div class="contact">
                    <a href="./contact" class="contact-button button" target="_blank">お問い合わせはこちら</a>
                </div>
                <div class="application">
                    <a href="./application" class="application-button button" target="_blank">お申し込みはこちら</a>
                </div>
            </div>
        </div>
    </section>
    <?php get_footer();?>