<section id="design">
        <h2>選べるデザイン</h2>
        <div class="wrapper">
            <div class="design-wrap first-line">
                <div class="design fade_off">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/design/1.png" alt="スタンダードA">
                    <p>スタンダードAパターン</p>
                    <div class="button"><a href="https://newstd.net/">デモ画面を見る</a></div>
                </div>
                <div class="design fade_off">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/design/2.png" alt="スタンダードB">
                    <p>スタンダードBパターン</p>
                    <div class="button"><a href="https://wp-emanon.jp/emanon-business/">デモ画面を見る</a></div>
                </div>
            </div>
            <div class="design-wrap second-line">
                <div class="design  fade_off">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/design/3.png" alt="コンサル">
                    <p>コンサルパターン</p>
                    <div class="button"><a href="http://demo-isotype.blue/majestic/">デモ画面を見る</a></div>
                </div>
                <div class="design  fade_off">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/design/4.png" alt="おしゃれ">
                    <p>おしゃれパターン</p>
                    <div class="button"><a href="http://demo-isotype.blue/monoidea/">デモ画面を見る</a></div>
                </div>
                <div class="design  fade_off">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/design/5.png" alt="カフェ">
                    <p>カフェパターン</p>
                    <div class="button"><a href="http://demo.crazy-wp.com/kanaka/">デモ画面を見る</a></div>
                </div>
            </div>
        </div>
    </section>