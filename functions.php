<?php

function wpcf7_custom_item_error_position( $items, $result ) {
	// メッセージを表示させたい場所のタグのエラー用のクラス名
	$class = 'wpcf7-custom-item-error';
	// メッセージの位置を変更したい項目名
	$names = array('your-name', 'tel-620', 'your-email', 'e-mail', 'your-subject', 'your-message');

	// 入力エラーがある場合
	if ( isset( $items['invalidFields'] ) ) {
		foreach ( $items['invalidFields'] as $k => $v ) {
			$orig = $v['into'];
			$name = substr( $orig, strrpos($orig, ".") + 1 );
			// 位置を変更したい項目のみ、エラーを設定するタグのクラス名を差替
			if ( in_array( $name, $names ) ) {
				$items['invalidFields'][$k]['into'] = ".{$class}.{$name}";
			}
		}
	}
	return $items;
}
add_filter( 'wpcf7_ajax_json_echo', 'wpcf7_custom_item_error_position', 10, 2 );
